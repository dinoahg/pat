package com.nghiado.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by nghiado on 1/22/2016.
 */
@Controller
public class HomeController {

    @RequestMapping("/")
    public String welcome(Model model) {

        model.addAttribute("greeting", "hello world!");
        return "welcome";
    }
}
